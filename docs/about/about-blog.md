# About This Blog

??? note "ChangeLog"
    ```text
    20190530:
        - 编写AboutThisBlog Page
    20190831:
        - 添加删除线插件
    ```

!!! abstract "TL;DR / 摘要"
    介绍了本博客的搭建过程、workflow等

## 博客技术架构

本来我前面是使用Wordpress来做了一个博客来试用的，Wordpress的确也提供了不错的功能，但是我感觉
还是操作比较麻烦不爽，加上本来那个博客也没写什么东西、而且是部署在腾讯云上，而我的域名是没法备案
的（无语了），所以重新搞一个博客咯。

后面看了下选择，感觉现在比较符合自己需求的就是用静态网站生成器来搞博客了，挑了一下后面觉得用
[Mkdocs](https://www.mkdocs.org/)吧。它自己的定位是写文档用的，所以提供的功能有限，当然了
有限的功能就表示需要更少的配置了，各有利弊吧，这个自己取舍就行了。

挑选了生成器后，就再选一下对于的主题吧，我找了一圈找了个自己看起来感觉可以的
[Material for MkDocs](https://squidfunk.github.io/mkdocs-material/)，然后我个人是不
喜欢HTML和Javascript的，所以拿来了主题后我直接就在项目中已经生成的模板文件中修改了一下就算了
（改了一下配色啊、删除了一些自己不想要的元素啊这些）。然后Mkdocs和Material的配置就是查找一下别人
的配置啊，自己尝试使用就配出来了，还是不复杂的。

Mkdocs生成后的是静态网站，那我就在VPS上使用lighttpd这个服务器软件来host一下就行了。

## 博客的使用workflow

然后我说一下我写博客的工作流吧。

首先在本地使用markdown来编写博客，本地就可以使用mkdocs的serve功能就可以即时预览了，比较方便。

写完后push到gitlab上的repo上，gitlab上设置了一个webhook来通知VPS更新博客，然后在VPS上使用
flask写了一个小的web应用来处理webhook。接到webhook过来的请求后，VPS调用脚本pull更新VPS上的
repo，然后在VPS上构建静态页面并且拷贝到www目录下面就可以了。

这个流程我个人觉得还是比较简单的，配置好了后以后只需要本地写博客，预览好后push过去其他流程就都是
自动化的了。

## [20190831]添加删除线插件

今天来正式的想写点东西记录点东西，才发现其实Mkdocs原生是不支持 ~~删除线（Strikethrough）~~ 的。
[对应的Github Issue Link](https://github.com/mkdocs/mkdocs/issues/868)

看着Issue上的讨论，因该是删除线就不是Markdown原生支持的，应该是属于后面扩展的语法，所以Mkdocs
也没有（也不会）原生支持，不过可以通过插件的方式解决，启用`pymdownx.tilde`插件就可以了。



--8<--
docs/.no_nav/footer.md
--8<--
