# About Me

??? note "ChangeLog"
    ```text
    20190530:
        - 创建AboutMe Page
    20190831：
        - Update了一下
    ```

## Alex Woo

![](../img/me.jpg)

AKA 吴颖聪

AKA alex-itboy

### 我是一个

- 计算机技术爱好者
- 软件工人，向工头方向发展
- <s>有芫荽气味的白切鸡狙击手</s>（已经没有时间来维持状态了咯。。。So sad）
- 有芫荽气味的车神葱


### 感兴趣的方向

- Linux、云技术
- 编程语言方面
- 操作系统方面
- 硬件嵌入式方面


### 我喜欢

- 看美剧
    - How I Met Your Mother
    - The Big Bang Theory
    - The Elementary
    - The Blacklist
- 打FPS游戏
    - CSGO(AFK)
- 养老开车游戏
    - 欧洲卡车模拟2
- 做白日梦、瞎想、YY


## 联系方式

- Gitlab
    - https://gitlab.com/alex-whybother/
- Github
    - https://github.com/alex-itboy
- Email
    - alex AT alexitboy.icu


--8<--
docs/_no_nav/footer.md
--8<--
