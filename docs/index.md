# 欢迎来到Alex的博客

## Welcome

欢迎来到我的博客，这里我会分享我的一些知识、经验，欢迎大家一起探索问题，如果我出现了错误欢迎指出
。可以通过gitlab、github或者页脚下面的邮箱来联系我，如果想进一步了解我或者本站可以查看下About
下面的Pages。

## 博文列表

--8<--
docs/.no_nav/blog-list.md
--8<--

## 友情链接

--8<--
docs/.no_nav/friends.md
--8<--

--8<--
docs/.no_nav/footer.md
--8<--
