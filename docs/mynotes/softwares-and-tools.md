# 常用的工具/软件

??? note "ChangeLog"
    ```text
    20200331:
        - 创建 常用的工具/软件 Page
        - add KeyTweak, SumatraPDF, Q-Dir, Notepad++, AutoHotKey
    ```

!!! abstract "TL;DR / 摘要"
    介绍一些好用的工具与软件(系统无关)

## 视频相关

### You-Get - 视频下载工具

- [You-Get](https://you-get.org/)

- #### 简介

    一个命令行下的视频下载软件，可以下载许多不同的常见的在线视频网站上的视频，我一般用来下载B站
    和Youtube上的一些视频，命令简单一看就会，非常方便！

--8<--
docs/.no_nav/footer.md
--8<--
