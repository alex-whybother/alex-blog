# 常用的网站

??? note "ChangeLog"
    ```text
    20200331:
        - 创建Page，初版内容
    ```

!!! abstract "TL;DR / 摘要"
    分享一些常用的网站，Awesome的网站等


## 编译

- [Compiler Explorer](https://godbolt.org/)
    - 在线使用不同版本的编译器对代码进行编译并显示汇编代码


## ASCII Art

- [asciiflow](http://asciiflow.com/)
    - 任意画图
- [Ascii Table](https://ozh.github.io/ascii-tables/)
    - 画表格（中文支持不太好）
- [Tables Generator](https://www.tablesgenerator.com/text_tables)
    - 画表格，中文支持好



--8<--
docs/.no_nav/footer.md
--8<--
