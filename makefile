# makefile for mkdocs to generate my blog to review on loacl
# a remote one will generate on remote server
blog:
	chromium http://127.0.0.1:8000/ >> /dev/null &
	mkdocs serve

build:
	mkdocs build

clean:
	rm site -rf

install:
	sudo pip3 install mkdocs  pygments pymdown-extensions
